package listeners;

import commons.TestBase;
import org.testng.ITestResult;
import org.testng.Reporter;
import org.testng.TestListenerAdapter;
import ru.yandex.qatools.ashot.AShot;
import ru.yandex.qatools.ashot.Screenshot;
import ru.yandex.qatools.ashot.shooting.ShootingStrategies;

import javax.imageio.ImageIO;
import java.io.File;
import java.io.IOException;
import java.util.Date;

public class ScreenshotListener extends TestListenerAdapter {

    @Override
    public void onTestFailure(ITestResult result) {
        try {
            takeScreeshot(result);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onTestSuccess(ITestResult result) {
        try {
            takeScreeshot(result);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    public void takeScreeshot(ITestResult result) throws IOException {
        Screenshot screenshot = new AShot().shootingStrategy(ShootingStrategies.viewportPasting(1000)).takeScreenshot(TestBase.driver);
        String path = System.getProperty("user.dir") + "/surefire-reports/ScreenShots/";
        Date date = new Date();
        String fileName = result.getTestClass().getRealClass().getSimpleName() + "_" + result.getMethod().getMethodName() + "_" + date.toString().replace(' ', '_').replace(':', '_').replace("_CET", "") + ".jpg";
        new File(path).mkdirs();
        ImageIO.write(screenshot.getImage(), "jpg", new File(path + fileName));
        Reporter.log("<a href='../ScreenShots/" + fileName + "'> <img src='../ScreenShots/" + fileName + "' border='3' height='200' width='200'/> </a>");
    }
}
