package commons;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.testng.ITestResult;
import org.testng.Reporter;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeSuite;
import ru.yandex.qatools.ashot.AShot;
import ru.yandex.qatools.ashot.Screenshot;
import ru.yandex.qatools.ashot.shooting.ShootingStrategies;

import javax.imageio.ImageIO;
import java.io.File;
import java.io.IOException;
import java.util.Date;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

public class TestBase {
    public static WebDriver driver;
    private Properties properties;

    @BeforeSuite
    public void setUp() {
        properties = new Properties();
        try {
            properties.load(this.getClass().getResourceAsStream("/main.properties"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        System.setProperty("webdriver.chrome.driver", properties.getProperty("chromedriver.path"));
        System.setProperty("org.uncommons.reportng.escape-output", "false");
        runBrowser();
    }

    @AfterSuite
    public void tearDown() {
        driver.quit();
    }

    private void runBrowser() {
        ChromeOptions options = new ChromeOptions();
        options.addArguments("--incognito");
        driver = new ChromeDriver(options);
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        driver.manage().window().maximize();
        driver.get("http://automationpractice.com/index.php");
    }

    public void takeScreeshot(ITestResult result) throws IOException {
        Screenshot screenshot = new AShot().shootingStrategy(ShootingStrategies.viewportPasting(1000)).takeScreenshot(TestBase.driver);
        String path = System.getProperty("user.dir") + "/surefire-reports/ScreenShots/";
        Date date = new Date();
        String fileName = result.getTestClass().getRealClass().getSimpleName() + "_" + result.getMethod().getMethodName() + "_" + date.toString().replace(' ', '_').replace(':', '_').replace("_CET", "") + ".jpg";
        new File(path).mkdirs();
        ImageIO.write(screenshot.getImage(), "jpg", new File(path + fileName));
        Reporter.log("<a href='../ScreenShots/" + fileName + "'> <img src='../ScreenShots/" + fileName + "' border='3' height='200' width='200'/> </a>");
    }

}
