package pages;

import commons.PageBase;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.testng.Assert;

import java.util.List;

public class PaymentsPage extends PageBase {

    public PaymentsPage(WebDriver driver) {
        super(driver);
    }

    @FindBy(css = "#HOOK_PAYMENT > div > div > p > a")
    private List<WebElement> paymentsList;

    @FindBy(css = "#center_column > form > div > h3")
    private WebElement paymentMethodTitle;

    @FindBy(css = "#cart_navigation > button")
    private WebElement confirmOrderButton;

    public void selectPaymentMethod(String paymentMethod) {
//        WebElement selectedPayment = paymentsList.stream().filter(x -> paymentMethod.equals(x.getAttribute("class"))).findFirst().
//                orElseThrow(() -> new AssertionError("Payment method " + paymentMethod + " was not found on payments page."));

        WebElement selectedPayment = null;
        for (WebElement payment: paymentsList) {
            if(paymentMethod.equals(payment.getAttribute("class"))) {
                selectedPayment = payment;
            }
        }
        if (selectedPayment == null) {
            throw new AssertionError("Payment method " + paymentMethod + " was not found on payments page.");
        }
        clickElement(selectedPayment, 5);
    }

    public void verifySelectedPaymetMethod(String paymentMethod) {
        String displayedPaymentMethodTitle = paymentMethodTitle.getText();
        String expectedPaymentMethodTitle = "";
        switch (paymentMethod) {
            case "bankwire":
                expectedPaymentMethodTitle = "BANK-WIRE PAYMENT.";
                break;
            case "cheque":
                expectedPaymentMethodTitle = "CHECK PAYMENT";
                break;
        }
        Assert.assertEquals(displayedPaymentMethodTitle, expectedPaymentMethodTitle, "Invalid payment method title.");
    }

    public void confirmOrder() {
        clickElement(confirmOrderButton, 5);
    }
}
