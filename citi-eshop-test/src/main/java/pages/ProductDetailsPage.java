package pages;

import commons.PageBase;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.Select;
import org.testng.ITestContext;

import java.util.List;

public class ProductDetailsPage extends PageBase {

    public ProductDetailsPage (WebDriver driver) {
        super(driver);
    }

    @FindBy(id = "quantity_wanted")
    private WebElement quantityInput;

    @FindBy(id = "group_1")
    private WebElement sizeSelectAsWebElement;

    @FindBy(css = "#color_to_pick_list > li > a")
    private List<WebElement> colorsList;

    @FindBy(css = "#add_to_cart > button")
    private WebElement addToCartButton;

    @FindBy(css = "div.button-container > span[title='Continue shopping']")
    private WebElement continueShoppingButton;

    @FindBy(css = "div.button-container > a[title='Proceed to checkout']")
    private WebElement proceedToCheckoutButton;

    @FindBy(id = "our_price_display")
    private WebElement productPrice;

    @FindBy(css = "h1[itemprop='name']")
    private WebElement productName;

    private void setQuantity(String quantity) {
        clearElement(quantityInput);
        sendKeysToElement(quantityInput, quantity);
    }

    private void selectSize(String size) {
        Select select = new Select(sizeSelectAsWebElement);
        select.selectByVisibleText(size);
    }

    private void selectColor(String color) {
//        WebElement colorLink = colorsList.stream().filter(x -> color.equals(x.getAttribute("name"))).findFirst().
//                orElseThrow(() -> new AssertionError("Color " + color+ " is not available for selected product."));
        WebElement colorLink = null;
        for (WebElement element: colorsList) {
            if (color.equals(element.getAttribute("name"))) {
                colorLink = element;
            }
        }
        if (colorLink == null) {
            throw new AssertionError("Color " + color+ " is not available for selected product.");
        }
        clickElement(colorLink, 1);
    }

    public void addProductToCart(String quantity, String size, String color, String continueShoppingString, ITestContext context) {
        setQuantity(quantity);
        selectSize(size);
        selectColor(color);
        String price = productPrice.getText().substring(1);
        saveProductDetailsToTestContext(context,productName.getText(), price, quantity);
        clickElement(addToCartButton, 1);
        Boolean continueShopping = Boolean.parseBoolean(continueShoppingString);
        if (continueShopping) {
            clickElement(continueShoppingButton, 5);
        } else {
            clickElement(proceedToCheckoutButton, 5);
        }
    }

    private void saveProductDetailsToTestContext(ITestContext context, String productName, String price, String quantity) {
        String formattedProductName = productName.replaceAll("\\s+", "");
        context.getSuite().setAttribute(formattedProductName + "_price", price);
        context.getSuite().setAttribute(formattedProductName + "_quantity", quantity);
    }
}
