package pages;

import commons.PageBase;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.testng.Assert;
import org.testng.ITestContext;
import org.testng.asserts.SoftAssert;

import java.util.List;

public class CartSummaryPage extends PageBase {

    public CartSummaryPage (WebDriver driver) {
        super(driver);
    }

    @FindBy(css = "table[id='cart_summary'] > tbody > tr")
    private List<WebElement> productsList;

    @FindBy (id = "total_product")
    private WebElement totalProductsValueElement;

    @FindBy (id = "total_shipping")
    private WebElement totalShippingValueElement;

    @FindBy(id = "total_price_without_tax")
    private WebElement totalCartValueWithoutTaxElement;

    @FindBy(id = "total_tax")
    private WebElement totalTaxValueElement;

    @FindBy(id = "total_price")
    private WebElement totalCartValueElement;

    @FindBy(css = "#center_column > p.cart_navigation.clearfix > a[title='Proceed to checkout']")
    private WebElement confirmCartButton;

    private String productNameSelector = "td.cart_description > p > a";
    private String unitPriceSelector = "td.cart_unit > span.price > span:nth-child(1)";
    private String quantitySelector = "td.cart_quantity.text-center > input:nth-child(2)";
    private String totalProductValueSelector = "td.cart_total > span";

    public void verifyCartSummary (ITestContext context) {
        SoftAssert softAssert = new SoftAssert();
        Float totalContextProductsValue = 0.0f;

        for (WebElement product: productsList) {
            String productName = product.findElement(By.cssSelector(productNameSelector)).getText();
            String formattedProductName = productName.replaceAll("\\s+", "");
            String contextUnitPrice = (String) context.getSuite().getAttribute(formattedProductName + "_price");
            Float contextUnitPriceFloat = Float.parseFloat(contextUnitPrice);
            String contextQuantityString = (String) context.getSuite().getAttribute(formattedProductName + "_quantity");
            Integer contextQuantityInteger = Integer.parseInt(contextQuantityString);
            totalContextProductsValue += contextUnitPriceFloat * contextQuantityInteger;

            String tableUnitPrice = product.findElement(By.cssSelector(unitPriceSelector)).getText().substring(1);
            String tableUnitQuantity = product.findElement(By.cssSelector(quantitySelector)).getAttribute("value");
            String tableTotalProductValue = product.findElement(By.cssSelector(totalProductValueSelector)).getText().substring(1);

            softAssert.assertEquals(tableUnitPrice, contextUnitPrice, "Invalid unit price for product: " + productName);
            softAssert.assertEquals(tableUnitQuantity, contextQuantityString, "Invalid unit quantity for product: " + productName);
            softAssert.assertEquals(Float.parseFloat(tableTotalProductValue), contextQuantityInteger * contextUnitPriceFloat, "Invalid total value for product: " + productName);
        }

        Float totalProductsValue = Float.parseFloat(totalProductsValueElement.getText().substring(1));
        Float totalShippingValue = Float.parseFloat(totalShippingValueElement.getText().substring(1));
        Float totalCartValueWithoutTax = Float.parseFloat(totalCartValueWithoutTaxElement.getText().substring(1));
        Float totalTaxValue = Float.parseFloat(totalTaxValueElement.getText().substring(1));
        Float totalCartValue = Float.parseFloat(totalCartValueElement.getText().substring(1));
        softAssert.assertEquals(totalProductsValue + 1, totalContextProductsValue, "Invalid total products value");
        softAssert.assertEquals(totalCartValueWithoutTax + 1, totalProductsValue + totalShippingValue, "Invalid total cart without tax value");
        softAssert.assertEquals(totalCartValue, totalCartValueWithoutTax + totalTaxValue, "Invalid total cart value");

        softAssert.assertAll();
    }

    public void proceedToCheckout() {
        clickElement(confirmCartButton, 5);
    }
}
