package pages;

import commons.PageBase;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import java.util.List;

public class ProductsPage extends PageBase {

    public ProductsPage(WebDriver driver) {
        super(driver);
    }

    private String productLinkSelector = "div.product-container > div.right-block > h5 > a";

    @FindBy(css = "#center_column > ul > li")
    private List<WebElement> productsList;

    public void selectProduct(String productName) {
//        WebElement product = productsList.stream().filter(x -> productName.equals(x.findElement(By.cssSelector(productLinkSelector)).getAttribute("title"))).findFirst().
//                orElseThrow(() -> new AssertionError("Product " + productName + " was not found on products list."));

        WebElement product = null;
        for (WebElement element: productsList) {
            String title = element.findElement(By.cssSelector(productLinkSelector)).getAttribute("title");
            if (productName.equals(title)) {
                product = element.findElement(By.cssSelector(productLinkSelector));
                break;
            }
        }
        if (product == null) {
            throw new AssertionError("Product " + productName + " was not found on products list.");
        }
        moveToElement(product);
        clickElement(product, 5);
    }

}
