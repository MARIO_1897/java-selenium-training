package pages;

import commons.PageBase;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class ShippingPage extends PageBase {

    public ShippingPage (WebDriver driver) {
        super(driver);
    }

    @FindBy(css = "#uniform-cgv > span")
    private WebElement acceptTermsCheckbox;

    @FindBy(css = "#form > p.cart_navigation.clearfix > button[name='processCarrier']")
    private WebElement acceptShippingButton;

    public void acceptShipping() {
        clickElement(acceptTermsCheckbox, 5);
        clickElement(acceptShippingButton, 5);
    }
}
