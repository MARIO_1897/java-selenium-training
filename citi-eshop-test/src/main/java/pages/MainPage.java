package pages;

import commons.PageBase;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import java.util.List;

public class MainPage extends PageBase {

    public MainPage (WebDriver driver) {
        super(driver);
    }

    @FindBy(css = "#header > div.nav > div > div > nav > div.header_user_info > a")
    private WebElement signInButton;

    @FindBy(css = "#header > div.nav > div > div > nav > div:nth-child(1) > a.account")
    private WebElement myAccountLink;

    @FindBy(css = "#block_top_menu > ul > li")
    private List<WebElement> mainMenuCategoies;

    private String categoryXpath = "./ul/li[./a[text()='%s']]/ul/li/a[text()='%s']";

    public void clickSignInButton() {
        clickElement(signInButton, 10);
    }

    public boolean checkMyAccountLinkVisibility() {
        return isElementVisible(myAccountLink, 10);
    }

    public void selectCategory(String mainMenuCategoryName, String categoryName, String subCategoryName){
//        WebElement mainMenuCategory = mainMenuCategoies.stream().filter(x -> mainMenuCategoryName.equals(x.getText())).findFirst().
//                orElseThrow(() -> new AssertionError("Category " + mainMenuCategoryName + " was not found in menu."));
        WebElement mainMenuCategory = null;
        for (WebElement element: mainMenuCategoies) {
            if (mainMenuCategoryName.equals(element.getText())) {
                mainMenuCategory = element;
                break;
            }
        }
        if (mainMenuCategory == null) {
            throw new AssertionError("Category " + mainMenuCategoryName + " was not found in menu.");
        }
        moveToElement(mainMenuCategory);
        mainMenuCategory.findElement(By.xpath(String.format(categoryXpath, categoryName, subCategoryName))).click();
    }
}
