package pages;

import commons.PageBase;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.testng.ITestResult;

import java.io.IOException;

public class SignInPage extends PageBase {

    public SignInPage(WebDriver driver) {
        super(driver);
    }

    @FindBy(id = "email")
    private WebElement emailInput;

    @FindBy(id = "passwd")
    private WebElement passwordInput;

    @FindBy(id = "SubmitLogin")
    private WebElement submitSignInButton;

    public void signIn(String email, String password) {
        sendKeysToElement(emailInput, email);
        sendKeysToElement(passwordInput, password);
        clickElement(submitSignInButton, 5);
    }

}
