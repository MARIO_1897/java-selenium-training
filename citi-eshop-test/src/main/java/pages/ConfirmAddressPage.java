package pages;

import commons.PageBase;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class ConfirmAddressPage extends PageBase {

    public ConfirmAddressPage (WebDriver driver) {
        super(driver);
    }

    @FindBy(css = "form > p.cart_navigation.clearfix > button[name='processAddress']")
    private WebElement confirmAddressButton;

    public void confirmAddress() {
        clickElement(confirmAddressButton, 5);
    }

}