package pages;

import commons.PageBase;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.testng.Assert;

public class OrderConfirmationPage extends PageBase {

    public OrderConfirmationPage(WebDriver driver) {
        super(driver);
    }

    @FindBy(css = "#center_column > p.alert.alert-success")
    private WebElement successAlert;

    public void verifySuccessAlertVisibility() {
        Assert.assertTrue(isElementVisible(successAlert, 10), "Success alert is not visible");
    }
}
