package eshop;

import commons.TestBase;
import org.testng.ITestContext;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;
import pages.*;

public class ConfirmOrderTest extends TestBase {

    @Test
    public void confirmCart(ITestContext context) {
        CartSummaryPage cartSummaryPage = new CartSummaryPage(driver);
        cartSummaryPage.verifyCartSummary(context);
        cartSummaryPage.proceedToCheckout();
    }

    @Test(dependsOnMethods = "confirmCart")
    public void confirmAddress() {
        ConfirmAddressPage confirmAddressPage = new ConfirmAddressPage(driver);
        confirmAddressPage.confirmAddress();
    }

    @Test(dependsOnMethods = "confirmAddress")
    public void confirmShipping() {
        ShippingPage shippingPage = new ShippingPage(driver);
        shippingPage.acceptShipping();
    }

    @Parameters({"payment"})
    @Test(dependsOnMethods = "confirmShipping")
    public void selectPayment (String paymentMethod) {
        PaymentsPage paymentsPage = new PaymentsPage(driver);
        paymentsPage.selectPaymentMethod(paymentMethod);
        paymentsPage.verifySelectedPaymetMethod(paymentMethod);
        paymentsPage.confirmOrder();
    }

    @Test(dependsOnMethods = "selectPayment")
    public void verifyOrderCofirmation() {
        OrderConfirmationPage orderConfirmationPage = new OrderConfirmationPage(driver);
        orderConfirmationPage.verifySuccessAlertVisibility();
    }
}
