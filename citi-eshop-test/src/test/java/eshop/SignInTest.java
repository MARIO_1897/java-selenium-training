package eshop;

import commons.TestBase;
import org.testng.Assert;
import org.testng.ITestContext;
import org.testng.ITestResult;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;
import pages.MainPage;
import pages.SignInPage;

import java.io.IOException;

public class SignInTest extends TestBase {

    @Parameters({"email", "password"})
    @Test
    public void signIn(String email, String password, ITestContext context, ITestResult result) throws IOException {
        MainPage mainPage = new MainPage(driver);
        mainPage.clickSignInButton();
        SignInPage signInPage = new SignInPage(driver);
        signInPage.signIn(email, password);
        Assert.assertTrue(mainPage.checkMyAccountLinkVisibility(), "My account link is not visible, signing in failed.");
//        takeScreeshot(result);
        context.getSuite().setAttribute("authentication", true);
    }
}
