package eshop;

import commons.TestBase;
import org.testng.ITestContext;
import org.testng.SkipException;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;
import pages.MainPage;
import pages.ProductDetailsPage;
import pages.ProductsPage;

public class AddProductTest extends TestBase {

    @Parameters({"menu", "category", "subCategory"})
    @Test
    public void selectCategory(String menu, String category, String subCategory, ITestContext context) throws InterruptedException {
        Boolean userAuthenticated = (Boolean) context.getSuite().getAttribute("authentication");
        if (userAuthenticated == null) {
            throw new SkipException("User is not authenticated, skipping product addition.");
        }
        MainPage mainPage = new MainPage(driver);
        mainPage.selectCategory(menu, category, subCategory);
        Thread.sleep(5000);
    }

    @Parameters({"productName"})
    @Test(dependsOnMethods = "selectCategory")
    public void selectProduct(String productName) {
        ProductsPage productsPage = new ProductsPage(driver);
        productsPage.selectProduct(productName);
    }

    @Parameters({"quantity", "size", "color", "continueShopping"})
    @Test(dependsOnMethods = "selectProduct")
    public void confirmProduct(String quantity, String size, String color, String continueShopping, ITestContext context) {
        ProductDetailsPage productDetailsPage = new ProductDetailsPage(driver);
        productDetailsPage.addProductToCart(quantity, size, color, continueShopping, context);
    }
}
