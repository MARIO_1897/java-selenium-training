package basic;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.Test;

import java.io.IOException;
import java.util.Properties;

public class BasicSeleniumTest {

    @Test
    public void browserTest() throws InterruptedException {
        Properties properties = new Properties();
        try {
            properties.load(this.getClass().getResourceAsStream("/main.properties"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        System.setProperty("webdriver.chrome.driver", properties.getProperty("chromedriver.path"));
        WebDriver driver = new ChromeDriver();
        driver.manage().window().maximize();
        driver.get("https://google.com");
        Thread.sleep(5000);
        driver.quit();
    }
}
