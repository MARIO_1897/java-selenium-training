package basic;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.Test;

import java.io.IOException;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

public class SignInTest {

    @Test
    public void signIn() throws InterruptedException {
        Properties properties = new Properties();
        try {
            properties.load(this.getClass().getResourceAsStream("/main.properties"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        System.setProperty("webdriver.chrome.driver", properties.getProperty("chromedriver.path"));
        WebDriver driver = new ChromeDriver();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        driver.manage().window().maximize();
        driver.get("http://automationpractice.com/index.php");
        driver.findElement(By.cssSelector("#header > div.nav > div > div > nav > div.header_user_info > a")).click();
        driver.findElement(By.id("email")).sendKeys("mariusz.lazor@test.com");
        driver.findElement(By.id("passwd")).sendKeys("test123");
        driver.findElement(By.id("SubmitLogin")).click();
        Thread.sleep(5000);
        driver.quit();
    }
}
